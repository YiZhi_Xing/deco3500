// 做出正确选择
var correctNum = 0;
// 错误选择
var wrongNum = 0;
// 当前几个场景 从0开始
var sceneNum = 0;
// 保存当前场景是否成功
var succeed = false;

// dom加载完
$(function() {
	// 所有场景的配置
	var sceneArr = [{
			dom: $('#scene1'),
			titleImg: './img/Task1.png',
			background: './img/background1.png',
			choiceNum: 2
		},
		{
			dom: $('#scene2'),
			titleImg: './img/Task2.png',
			background: './img/background2.png',
			choiceNum: 1

		}
	];
	// 倒计时
	var timer = null;

	function countdown() {
		var time = 300;
		var timeEle = $('#countdown');
		timer = setInterval(function() {
			time--;
			timeEle.text(time + ' s');
			// 如果到时间0了清除定时器
			if (time == 0) {
				// 错误次数直接加10
				wrongNum += 10;
				judge();
			}
		}, 1000);
	}

	// 开始页面点击
	$('#startButton').on('click', function() {
		// 启动设置为300秒
		$('#countdown').text(300 + ' s');
		$('#start').fadeOut();
		// 启动倒计时
		countdown();
	});
	// 结束页面点击new game
	$('#endButton').on('click', function() {
		// 判断当前场景是否是否成功
		if (sceneNum == 0 && succeed) {
			// 当前场景任务成功
			setTimeout(function() {
				// 隐藏场景当前元素
				sceneArr[sceneNum].dom.hide();
				// 下一个场景初始化
				sceneNum++;
				// 将游戏开始界面设置为下个场景
				$('#start .title').prop('src', sceneArr[sceneNum].titleImg);
				// 设置下个场景的游戏背景
				$('#background').css('background', 'url(' + sceneArr[sceneNum].background + ') no-repeat center/cover');
				// 显示第下个场景的隐藏按钮
				sceneArr[sceneNum].dom.show();
				// 隐藏成功界面
				$('#end').hide();
				// 显示下个场景开始页面
				$('#start').show();
			}, 400);
		} else {
			// 当前场景任务失败
			setTimeout(function() {
				if(sceneNum == 0 || succeed) {
					location.reload();
				} else {
					// 隐藏失败界面
					$('#end').hide();
					$('#start').show();
				}
				
			}, 400);
		}


	});
	// 点击普通按钮声效
	$('#endButton,#startButton,.mdui-btn,.countdown-box,#quit,.btn1').on('click', function() {
		// 播放音效
		$('#audio').prop('src', './mp3/Click2.mp3');
	});
	// 点击事物按钮声效
	$('.btn').on('click', function() {
		// 播放音效
		$('#audio').prop('src', './mp3/Click1.mp3');
	});



	// 场景1 start
	// 获取游戏时长选择的对话框
	var dialog5 = new mdui.Dialog('#dialog5', {
		modal: true
	});


	// 获取游戏时长选择的对话框关闭
	$('#dialog5')[0].addEventListener('closed.mdui.dialog', function() {
		judge();
	});


	// 获取失眠选择的对话框
	var dialog4 = new mdui.Dialog('#dialog4', {
		modal: true
	});

	// 失眠选择的对话框关闭
	$('#dialog4')[0].addEventListener('closed.mdui.dialog', function() {
		judge();
	});
	// 第二层对话框的正确选择
	$('#dialog4-btn1,#dialog5-btn2').on('click', function() {
		correctNum++;
	});
	// 第二层对话框的错误选择
	$('#dialog4-btn2,#dialog4-btn3,#dialog5-btn1').on('click', function() {
		wrongNum++;
	});
	// 电脑点击确定
	$('#game')[0].addEventListener('confirm.mdui.dialog', function() {
		correctNum++;
	});
	// 电脑点击取消
	$('#game')[0].addEventListener('cancel.mdui.dialog', function() {
		dialog5.open();
	});
	// 电脑对话框关闭
	$('#game')[0].addEventListener('closed.mdui.dialog', function() {
		// 隐藏按钮
		$('#computer').hide();
		judge();
	});


	// 床点击确定
	$('#sleep')[0].addEventListener('confirm.mdui.dialog', function() {

		$('#tip1').fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				dialog4.open();
			});
		});
	});
	// 床点击取消
	$('#sleep')[0].addEventListener('cancel.mdui.dialog', function() {
		wrongNum++;
	});
	// 床对话框关闭
	$('#sleep')[0].addEventListener('closed.mdui.dialog', function() {
		// 隐藏按钮
		$('#bad').hide();
		judge();
	});
	// 场景1 end
	// 场景2 start
	// 对话框6
	var dialog6 = new mdui.Dialog('#scene2-dialog6', {
		modal: true
	});
	// 对话框7
	var dialog7 = new mdui.Dialog('#scene2-dialog7', {
		modal: true
	});
	// 对话框8
	var dialog8 = new mdui.Dialog('#scene2-dialog8', {
		modal: true
	});
	// 对话框9
	var dialog9 = new mdui.Dialog('#scene2-dialog9', {
		modal: true
	});
	// 二提示框的图片
	var tip2Img = $('#scene2-tip2');
	// 提示框盒子
	var tip2 = $('#tip2');
	// 点击男人的时候
	$('#man').on('click', function() {
		dialog6.open();
	});
	// 点击你认识苏珊嘛
	$('#scene2-dialog6-btn2').on('click', function() {
		tip2Img.prop('src', './img/iknowher.png')
		// 显示提示框
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				dialog7.open();
			});
		});
	});
	// 我想了解一下苏珊的生日 我想问一下她喜欢什么？
	$('#scene2-dialog7-btn1,#scene2-dialog7-btn2').on('click', function() {
		tip2Img.prop('src', './img/idontknow.png');
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				// 错误选择+1
				wrongNum++;
				judge();
			});
		});
	});
	// 上一个选择
	// 打扰一下，请问有空吗
	$('#scene2-dialog6-btn1').on('click', function() {
		tip2Img.prop('src', './img/whatsthematter.png')
		// 显示提示框
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				dialog8.open();
			});
		});
	});
	// 你是不是也喜欢苏珊？
	$('#scene2-dialog8-btn1').on('click', function() {
		tip2Img.prop('src', './img/whatthehell--.png');
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				// 错误选择+1
				wrongNum++;
				judge();
			});
		});
	});
	// 我想问一下你和苏珊很熟吗？
	$('#scene2-dialog8-btn2').on('click', function() {
		tip2Img.prop('src', './img/sorryidontknow.png');
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				// 错误选择+1
				wrongNum++;
				judge();
			});
		});
	});
	// 我是另外一个班级的，之前见到过你打篮球，看起来很酷！
	$('#scene2-dialog8-btn3').on('click', function() {
		tip2Img.prop('src', './img/hahathaks.png')
		// 显示提示框
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				dialog9.open();
			});
		});
	});
	// 好的！对了，你认识苏珊这个人吗，我找她有点事情
	$('#scene2-dialog9-btn1').on('click', function() {
		tip2Img.prop('src', './img/ofcourse.png');
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				// 正确选择+1
				correctNum++;
				judge();
			});
		});
	});

	// 场景2 end



	// 给离开弹出对话框
	var dialog3 = $('#dialog3');
	$('#quit').on('click', function() {
		dialog3.slideDown();
	});
	// 给第三个对话框注册点击事件
	// 重置
	$('#reset').on('click', function() {
		correctNum = 0;
		wrongNum = 0;
		clearInterval(timer);
		countdown();
		// 如果当前是场景0
		if (sceneNum == 0) {
			$('#computer').show();
			$('#bad').show();
		}
		dialog3.slideUp();
	});
	$('#view-task').on('click', function() {
		setTimeout(function() {
			location.reload();
		}, 400);
	});


	// 判断结果
	function judge() {
		// 没有大于当前场景选择次数退出
		if (correctNum + wrongNum < sceneArr[sceneNum].choiceNum) return;
		// 修改页面显示结果 播放音效
		var result = $('#result');
		var audio = $('#audio');
		if (wrongNum > 0) {
			// 失败
			succeed = false;
			if(!sceneNum){
				result.prop('src', './img/defeat.png');
			}else{
				result.prop('src', './img/defeat-1.png');
			}
			// 播放音效
			audio.prop('src', './mp3/Game Over.mp3');
		} else {
			// 成功
			succeed = true;
			if(!sceneNum){
				result.prop('src', './img/success.png');
			}else{
				result.prop('src', './img/Success-1.png');
			}
			// 播放音效
			audio.prop('src', './mp3/Success.mp3');
		}
		// 清除定时器
		clearInterval(timer);
		// 将选择 正确 错误 次数归零
		correctNum = 0;
		wrongNum = 0;
		// 显示页面
		$('#end').css({
			display: 'flex',
			opacity: '0'
		});
		$("#end").animate({
			opacity: 1
		}, 1000);

	}
});
